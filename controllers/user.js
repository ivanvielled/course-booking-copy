const User = require('../models/user')
const Course = require('../models/course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//check if email already exists
module.exports.emailExists = (params) => {
	return User.find({
		email: params.email
	}).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
}

//registration
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
		//hashSync() encrypts the password, and the 10 makes it happen 10 times
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
	})
}