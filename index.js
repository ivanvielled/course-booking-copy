//dependencies setup
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

//routes
const courseRoutes = require('./routes/course')
const userRoutes = require('./routes/user')

//database connection
mongoose.connection.once('open', () => console.log("Now connected to mongoDB Atlas"))

mongoose.connect('mongodb+srv://mongodbcluster:mongodbcluster@cluster0.ceomo.mongodb.net/course_booking?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//server setup
const app = express()
const port = 3000

//bodyparser middleware
app.use(express.json()) //only looks at request
app.use(express.urlencoded({extended: true})) //allows POST request to include nested objects

//cors configuration
// const corsOptions = {
// 	origin: 'http://localhost:3000',
// 	optionSuccessStatus: 200
// }
app.use(cors()) //this will be used if other sites try to access our API

//add all the routes
app.use('/api/courses', courseRoutes)
app.use('/api/users', userRoutes)

//server listening
app.listen(port, () => console.log(`Listening to port ${port}`))