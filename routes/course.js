const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/course')

//get all courses
router.get('/', (req, res) => {
	CourseController.getAllActive().then(resultsFromFind => res.send(resultsFromFind))
})

//get a single course
router.get('/:id', (req, res) => {
	let id = req.params.id
	CourseController.get({id}).then(resultFromFindById => res.send(resultFromFindById))
})

//create a new course
router.post('/', (req, res) => {
	CourseController.add(req.body).then(resultFromAdd => res.send(resultFromAdd))
})

//update a course
router.put('/', (req, res) => {
	CourseController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})

//archive(delete) a course
router.delete('/:id', (req, res) => {
	let id = req.params.id
	CourseController.archive({id}).then(resultFromArchive => res.send(resultFromArchive))
})

module.exports = router