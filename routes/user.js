const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

//Check if email exists
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//POST REGISTRATION
router.post('/', (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//POST LOGIN
router.post('/login', (req, res) => {
	res.send({type: "POST"})
	console.log("LOGIN")
})

//GET DETAILS
router.get('/details', (req, res) => {
	res.send({type: "GET"})
	console.log("DETAILS")
})

//POST ENROLL
router.post('/enroll', (req, res) => {
	res.send({type: "POST"})
	console.log("ENROLL")
})

module.exports = router